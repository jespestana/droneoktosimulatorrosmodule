//////////////////////////////////////////////////////
//  okto_like_simulator_ROSmoduleNode.cpp
//
//  Created on: May 22, 2014
//      Author: jespestana, joselusl
//
//////////////////////////////////////////////////////


//I/O stream
//std::cout
#include <iostream>

//ROS
#include "ros/ros.h"

//parrotARDrone
#include "OktoType/okto_like_simulator_ROSmodule.h"

//Comunications
#include "nodes_definition.h"


using namespace std;

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, MODULE_NAME_PELICAN_LIKE_SIMULATOR);
    ros::NodeHandle n;

    cout<<"[ROSNODE] Starting dronePelicanLikeSimulator"<<endl;

    // Drone Simulator Configuration File
    std::string drone_sim_config_filename;
    ros::param::get("~drone_sim_config_filename_str", drone_sim_config_filename);

    //Vars
    OktoLikeSimulatorROSModule MyDroneOktoLikeSimulatorROSModule( drone_sim_config_filename );
    MyDroneOktoLikeSimulatorROSModule.open(n,MODULE_NAME_PELICAN_LIKE_SIMULATOR);

    try
    {
        while(ros::ok())
        {
            //Read messages
            ros::spinOnce();

            //run
            MyDroneOktoLikeSimulatorROSModule.run();

            //Sleep
            MyDroneOktoLikeSimulatorROSModule.sleep();

        }
        return 1;

    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
}
