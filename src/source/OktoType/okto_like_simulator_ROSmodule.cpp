#include "OktoType/okto_like_simulator_ROSmodule.h"

OktoLikeSimulatorROSModule::OktoLikeSimulatorROSModule(const std::string &drone_config_filename) :
    DroneModule(droneModule::active, DRONE_PELICAN_LIKE_SIMULATOR_RATE) ,
    MyDroneSimulator(idDrone,stackPath,drone_config_filename)
{
    init();
    battery_timer.restart(false);
}

OktoLikeSimulatorROSModule::~OktoLikeSimulatorROSModule()
{
}

void OktoLikeSimulatorROSModule::init()
{
}

void OktoLikeSimulatorROSModule::close()
{
}

void OktoLikeSimulatorROSModule::open(ros::NodeHandle &nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Subscribers
    control_input_subscriber      = n.subscribe(DRONE_OKTO_LIKE_SIMULATOR_OKTO_COMMANDS_SUBSCRIBER, 1, &OktoLikeSimulatorROSModule::controlInputCallback, this);

    //Publishers
    ground_speed_sensor_publisher = n.advertise<px_comm::OpticalFlow>(          DRONE_OKTO_LIKE_SIMULATOR_GROUND_SPEED_SENSOR_PUBLISHER, 1, true);
    okto_sensor_data_publisher    = n.advertise<okto_driver::OktoSensorData>(   DRONE_OKTO_LIKE_SIMULATOR_OKTO_SENSOR_DATA_PUBLISHER   , 1, true);
    dronePosePubl                 = n.advertise<droneMsgsROS::dronePoseStamped>(DRONE_PELICAN_LIKE_SIMULATOR_DRONEPOSE_PUBLISHER       , 1, true);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

bool OktoLikeSimulatorROSModule::resetValues()
{
    return true;
}

bool OktoLikeSimulatorROSModule::startVal()
{
    return true;
}

bool OktoLikeSimulatorROSModule::stopVal()
{
    return true;
}

bool OktoLikeSimulatorROSModule::publishGroundSpeedSensorData()
{
    if(droneModuleOpened==false)
        return false;

    //Fill the msg
    px_comm::OpticalFlow optical_flow_msg;
    optical_flow_msg.header.stamp=ros::Time::now();
    optical_flow_msg.quality = 1;
    optical_flow_msg.flow_x  = 0;
    optical_flow_msg.flow_y  = 0;
    {
    double altitudeOut, varAltitudeOut;
    MyDroneSimulator.altitudeSensor.getAltitude( altitudeOut, varAltitudeOut);
    double vxOut, vyOut;
    MyDroneSimulator.groundSpeedSensor.getGroundSpeed( vxOut, vyOut);

    // m,   altitude
    optical_flow_msg.ground_distance = (float) -altitudeOut;
    // m/s,  mavwork reference frame
    optical_flow_msg.velocity_x      = (float)  vxOut;
    optical_flow_msg.velocity_y      = (float)  vyOut;
    }

    //Publish
    ground_speed_sensor_publisher.publish(optical_flow_msg);
    return true;
}

bool OktoLikeSimulatorROSModule::publishOktoSensorData()
{
    if(droneModuleOpened==false)
        return false;

    //Fill the msg
    okto_driver::OktoSensorData okto_sensor_data_msg;

    // rotation angles
    {
        double yaw_deg, pitch_deg, roll_deg;
        MyDroneSimulator.rotationAnglesSensor.getRotationAngles( yaw_deg, pitch_deg, roll_deg);
        okto_sensor_data_msg.ang_nick = (-1.0) * (10.0) * pitch_deg; // current pitch, ang_nick/10.0 = pitch_deg, ang_nick is in ddeg
        okto_sensor_data_msg.ang_roll = (+1.0) * (10.0) * roll_deg;  // current roll , units same as ang_nick
        okto_sensor_data_msg.yaw_gyro = (-1.0) * (10.0) * yaw_deg;   // current yaw  , units same as ang_nick
    }

    // okto_sensor_data_msg.voltage		# autopilot_battery voltage, V, voltios
#ifdef DRONE_PELICAN_LIKE_SIMULATOR_ACTIVATE_BATTERY_TIME
    double current_battery_time = battery_timer.getElapsedSeconds();
    double current_simulated_battery_level = 13.2 + (16.8 - 13.2) * (1.0 - current_battery_time/DRONE_PELICAN_LIKE_SIMULATOR_BATTERY_TIME);
    if (current_simulated_battery_level<0) current_simulated_battery_level = 0;
    okto_sensor_data_msg.voltage = current_simulated_battery_level; // V
#else
    okto_sensor_data_msg.voltage = 16.0; // V
#endif // DRONE_PELICAN_LIKE_SIMULATOR_ACTIVATE_BATTERY_TIME

    okto_sensor_data_msg.acc_nick	 		  = 0;
    okto_sensor_data_msg.acc_roll		      = 0;
    okto_sensor_data_msg.altitude			  = 0;
    okto_sensor_data_msg.acc_z                = 0;
    okto_sensor_data_msg.compass_heading	  = 0;
    okto_sensor_data_msg.gyro_compass_heading = 0;
    okto_sensor_data_msg.current			  = 0;
    okto_sensor_data_msg.capacity			  = 0;
    okto_sensor_data_msg.gps_nick			  = 0;
    okto_sensor_data_msg.gps_roll		      = 0;

    //Publish
    okto_sensor_data_publisher.publish(okto_sensor_data_msg);
    return true;
}

int OktoLikeSimulatorROSModule::publishDronePose()
{
    if(droneModuleOpened==false)
        return false;

    droneMsgsROS::dronePoseStamped estimated_pose_msg;

    double x, y, z, yaw, pitch, roll;
    MyDroneSimulator.getPosition_drone_GMR_wrt_GFF(x, y, z, yaw, pitch, roll);

    estimated_pose_msg.pose.pitch = static_cast<float>(pitch);
    estimated_pose_msg.pose.roll  = static_cast<float>(roll);
    estimated_pose_msg.pose.yaw   = static_cast<float>(yaw);
    estimated_pose_msg.pose.x     = static_cast<float>(x);
    estimated_pose_msg.pose.y     = static_cast<float>(y);
    estimated_pose_msg.pose.z     = static_cast<float>(z);

    estimated_pose_msg.header.stamp=ros::Time::now();
    estimated_pose_msg.pose.reference_frame = "GFF";
    estimated_pose_msg.pose.YPR_system      = "drone_GMR";
    estimated_pose_msg.pose.target_frame    = "wYvPuR";

    dronePosePubl.publish(estimated_pose_msg);
    return 1;
}

bool OktoLikeSimulatorROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    //Evaluate DroneSimulator
    MyDroneSimulator.run();

    //Publish everything
    publishGroundSpeedSensorData();
    publishOktoSensorData();
    publishDronePose();

    //End
    return true;
}

void OktoLikeSimulatorROSModule::controlInputCallback(const okto_driver::OktoCommand::ConstPtr& msg)
{
    double pitch_cmd  = 0;
    double roll_cmd   = 0;
    double dyaw_cmd   = 0;
    double thrust_cmd = 0;

    // okto
    thrust_cmd = (+1.0) * static_cast<double>(msg->gas);
    dyaw_cmd   = (+1.0) * static_cast<double>(msg->dyaw);
    roll_cmd   = (-1.0) * static_cast<double>(msg->roll);
    pitch_cmd  = (-1.0) * static_cast<double>(msg->nick);

    // saturate pitch
    if (pitch_cmd < -127) {
        pitch_cmd = -127;
    } else {
        if (127 < pitch_cmd) {
            pitch_cmd = 127;
        }
    }

    // saturate roll
    if (roll_cmd < -127) {
        roll_cmd = -127;
    } else {
        if (127 < roll_cmd) {
            roll_cmd = 127;
        }
    }

    // saturate dyaw
    if (dyaw_cmd < -127) {
        dyaw_cmd = -127;
    } else {
        if (127 < dyaw_cmd) {
            dyaw_cmd = 127;
        }
    }

    // saturate thrust
    if (thrust_cmd < 0) {
        thrust_cmd = 0;
    } else {
        if (255 < thrust_cmd) {
            thrust_cmd = 255;
        }
    }

    MyDroneSimulator.setInputs( pitch_cmd, roll_cmd, dyaw_cmd, thrust_cmd);


}
